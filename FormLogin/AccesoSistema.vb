﻿Imports System.Text.RegularExpressions

Public Class AccesoSistema

    Dim cuenta() As String = {"juan.barra@virginiogomez.cl", "ricardo.gonzalez3@virginiogomez.cl", "maurilinux@gmail.com"}
    Dim password() As String = {"password", "macoy123", "123456789"}
    Dim contador As Integer
    Const Intentos As Integer = 3

    Private Sub Button_access_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles button_access.Click

        For i As Integer = 0 To 2

            If Intento_exitoso() Then

                VentaPasajes.Show()
                Exit Sub
            Else
                Intento_fallido()
                Exit Sub
            End If

        Next

    End Sub

    Private Sub TextBox_cuenta_LostFocus(sender As Object, e As EventArgs) Handles textBox_cuenta.LostFocus
        If Email_valido() = False Then
            Email_incorrecto()
        End If
    End Sub

    Function Intento_exitoso() As Boolean

        Dim i As Integer

        If textBox_cuenta.Text.ToLower = cuenta(i) AndAlso textBox_password.Text.ToLower = password(i) Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub Intento_fallido()

        If (contador + 1 = Intentos) Then
            MessageBox.Show("Maxima cantidad de intentos fallidos alcanzada, adios.")
            Close()

        ElseIf contador < Intentos Then
            MessageBox.Show("Le quedan " + CStr(Intentos - (contador + 1)) + " intento")
            contador += 1

        End If

    End Sub

    Function Email_valido() As Boolean
        Dim v_email As Boolean = IsValidEmail(textBox_cuenta.Text)
        If v_email Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub Email_incorrecto()
        MessageBox.Show("Verifique Email Formato Incorrecto")
        textBox_cuenta.Focus()
    End Sub

End Class
