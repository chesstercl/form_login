﻿Public Class VentaPasajes
    Dim viaje(4, 4) As Integer
    Dim ciudades() As String = {"Concepción", "Los Angeles", "Temuco", "Puerto Montt"}

    Private Sub VentaPasajes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AccesoSistema.Close()
        Asignar_precios()
        Agregar_origen()
        Agregar_destino()
        TextBox_Disabled()
        TextBox_ReadOnly()
        Efectivo_disabled()
        Forma_de_Pago_Disabled()

    End Sub

    Private Sub Variaciones_viaje(sender As Object, e As EventArgs) Handles comboBox_origen.SelectedIndexChanged,
            comboBox_destino.SelectedIndexChanged, numericUpDownViajan.ValueChanged
        If Viaje_valido() Then
            Forma_de_Pago_Enabled()
            Valor_total()
        Else
            Forma_de_Pago_Disabled()
        End If

    End Sub

    Private Sub Efectivo_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButtonEfectivo.CheckedChanged

        If Medio_Pago_Efectivo() Then
            Efectivo_enabled()
        Else
            Efectivo_disabled()
        End If

    End Sub

    Private Sub Button_vuelto_Click(sender As Object, e As EventArgs) Handles button_vuelto.Click

        Obtener_vuelto()

    End Sub

    Private Sub Button_venta_Click(sender As Object, e As EventArgs) Handles button_venta.Click
        If Medio_Pago_Efectivo() AndAlso Obtener_vuelto() Then
            Mensaje_venta()
            Limpiar_formulario()
            Exit Sub
        ElseIf Forma_de_Pago_Checked() AndAlso Medio_Pago_Efectivo() = False Then
            Mensaje_venta()
            Limpiar_formulario()

            Exit Sub
        Else
            MessageBox.Show("Faltan datos por completar")
        End If
    End Sub

    Private Sub TextBox_efectivo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles textBox_efectivo.KeyPress
        SoloNumeros(e)
    End Sub

    Function Viaje_valido() As Boolean
        If Viajan() > 0 AndAlso Viaje_realizable() Then
            Return True
        Else
            Return False
        End If
    End Function

    Function Obtener_vuelto() As Boolean
        Dim total, efectivo, v_vuelto As Double

        total = Valor_total()
        efectivo = Pago_efectivo()

        If efectivo < total Then
            MessageBox.Show("Debe ingresar a los menos " + CStr(Valor_total()))
            Return False
        Else
            v_vuelto = efectivo - total
            textBox_vuelto.Text = v_vuelto
            Return True
        End If
    End Function

    Function Forma_de_Pago()
        Dim pago As String = ""
        If RadioButtonEfectivo.Checked Then
            pago = "Efectivo"
        ElseIf RadioButtonRedCompra.Checked Then
            pago = "RedCompra"
        ElseIf RadioButtonCredito.Checked Then
            pago = "Credito"

        End If
        Return pago

    End Function

    Function Valor_total()
        Dim valor_unitario As Integer
        Dim cantidad_viajan As Decimal

        valor_unitario = Precio_Unitario()
        cantidad_viajan = Viajan()

        Return Asignar_total(valor_unitario, cantidad_viajan)
    End Function

    Function Precio_viaje()
        Dim precio As Integer
        precio = viaje(comboBox_origen.SelectedIndex, comboBox_destino.SelectedIndex)
        Return precio
    End Function

    Public Function Asignar_total(ByVal valor_unitario As Double, ByVal cantidad As Decimal)
        Dim valor_total As Double

        valor_total = valor_unitario * cantidad
        textBox_total.Text = CStr(valor_total)

        Return valor_total
    End Function

    Public Function Viajan()
        Dim valor As Decimal
        valor = numericUpDownViajan.Value
        Return valor
    End Function

    Function Viaje_realizable() As Boolean
        If Ciudad_diferente() AndAlso Ida_y_vuelta() Then
            Return True
            Exit Function
        ElseIf Ciudad_diferente() = False Then
            Total_null()
            Unitario_null()
            Return False
            Exit Function
        Else
            Return False
        End If
    End Function

    Function Ciudad_diferente() As Boolean

        If (comboBox_destino.SelectedItem = comboBox_origen.SelectedItem) Then
            MessageBox.Show("Debe elegir destino y origen diferentes")
            Return False
        Else
            Return True
        End If
    End Function

    Function Ida_y_vuelta() As Boolean

        If (comboBox_destino.SelectedIndex = -1 Or comboBox_origen.SelectedIndex = -1) Then
            MessageBox.Show("Debe elegir destino y origen")
            Return False
        Else
            Return True
        End If
    End Function

    Sub Agregar_origen()
        For index = 0 To 3
            comboBox_origen.Items.Add(ciudades(index))
        Next
    End Sub

    Sub Agregar_destino()
        For index = 0 To 3
            comboBox_destino.Items.Add(ciudades(index))
        Next
    End Sub

    Function Pago_efectivo()
        Dim efectivo As Double

        If textBox_efectivo.Text <> "" Then

            efectivo = CDbl(textBox_efectivo.Text)
        End If
        Return efectivo
    End Function

    Public Function Precio_Unitario() As Integer
        Dim precio As Double

        If Viaje_realizable() Then
            precio = Precio_viaje()

        Else
            precio = 0
        End If
        textBox_unitario.Text = precio

        Return precio
    End Function

    Function Medio_Pago_Efectivo() As Boolean
        If RadioButtonEfectivo.Checked Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub Pago_efectivo_null()
        textBox_efectivo.Text = ""
    End Sub

    Sub Vuelto_null()
        textBox_vuelto.Text = ""
    End Sub

    Sub Total_null()
        textBox_total.Text = ""
    End Sub

    Sub Unitario_null()
        textBox_unitario.Text = ""
    End Sub

    Sub Efectivo_enabled()
        textBox_efectivo.Enabled = True
        textBox_efectivo.ReadOnly = False
        button_vuelto.Enabled = True
    End Sub

    Sub Efectivo_disabled()
        textBox_efectivo.Enabled = False
        textBox_efectivo.ReadOnly = True
        button_vuelto.Enabled = False
        Pago_efectivo_null()
        Vuelto_null()

    End Sub

    Sub TextBox_Disabled()
        textBox_unitario.Enabled = False
        textBox_total.Enabled = False
        textBox_efectivo.Enabled = False
        textBox_vuelto.Enabled = False
    End Sub

    Sub TextBox_ReadOnly()
        textBox_unitario.ReadOnly = True
        textBox_total.ReadOnly = True
        textBox_efectivo.ReadOnly = True
        textBox_vuelto.ReadOnly = True
    End Sub

    Sub Forma_de_Pago_Enabled()

        RadioButtonCredito.Enabled = True
        RadioButtonEfectivo.Enabled = True
        RadioButtonRedCompra.Enabled = True

    End Sub

    Sub Forma_de_Pago_Disabled()
        RadioButtonCredito.Enabled = False
        RadioButtonEfectivo.Enabled = False
        RadioButtonRedCompra.Enabled = False
        RadioButtonCredito.Checked = False
        RadioButtonRedCompra.Checked = False
        RadioButtonEfectivo.Checked = False

    End Sub

    Function Forma_de_Pago_Checked() As Boolean
        If RadioButtonEfectivo.Checked Then
            Return True
        ElseIf RadioButtonRedCompra.Checked Then
            Return True
        ElseIf RadioButtonCredito.Checked Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub Asignar_precios()
        viaje(0, 0) = 0
        viaje(0, 1) = 2500  'Concepcion a Los Angeles
        viaje(0, 2) = 7000  'Concepcion a Temuco
        viaje(0, 3) = 15000 'Concepcion a Puerto Montt

        viaje(1, 0) = 2500  'Los Angeles a Concepcion
        viaje(1, 1) = 0
        viaje(1, 2) = 5000  'Los Angeles a Temuco
        viaje(1, 3) = 12000 'Los Angeles a Puerto Montt

        viaje(2, 0) = 7000  'Temuco a Concepcion
        viaje(2, 1) = 5000  'Temuco a Los Angeles
        viaje(2, 2) = 0
        viaje(2, 3) = 6000  'Temuco a Puerto Mont

        viaje(3, 0) = 15000 'Puerto Montt a Concepcion
        viaje(3, 1) = 12000 'Puerto Montt a Los Angeles
        viaje(3, 2) = 6000  'Puerto Montt a Temuco
        viaje(3, 3) = 0
    End Sub

    Sub Mensaje_venta()
        MessageBox.Show("Resumen de venta    " & vbNewLine & vbNewLine &
                "Origen:             " & comboBox_origen.Text & vbNewLine &
                "Destino:            " & comboBox_destino.Text & vbNewLine &
                "Valor unitario:     " & Precio_Unitario() & vbNewLine &
                "Cantidad de pasajes:" & Viajan() & vbNewLine &
                "Total:              " & Valor_total() & vbNewLine &
                "Forma de pago:      " & Forma_de_Pago() & vbNewLine & vbNewLine &
                "Recuerde de utilizar cinturón de seguridad" & vbNewLine &
                "Que disfrute su viaje")
    End Sub

    Sub Limpiar_formulario()
        numericUpDownViajan.Value = 0
        textBox_unitario.Text = ""
        textBox_total.Text = ""
        textBox_efectivo.Text = ""
        textBox_vuelto.Text = ""
        Forma_de_Pago_Disabled()
        comboBox_destino.SelectedIndex = -1
        comboBox_origen.SelectedIndex = -1
    End Sub

End Class