﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VentaPasajes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.comboBox_origen = New System.Windows.Forms.ComboBox()
        Me.comboBox_destino = New System.Windows.Forms.ComboBox()
        Me.textBox_unitario = New System.Windows.Forms.TextBox()
        Me.textBox_total = New System.Windows.Forms.TextBox()
        Me.textBox_efectivo = New System.Windows.Forms.TextBox()
        Me.textBox_vuelto = New System.Windows.Forms.TextBox()
        Me.button_vuelto = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.numericUpDownViajan = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioButtonCredito = New System.Windows.Forms.RadioButton()
        Me.RadioButtonRedCompra = New System.Windows.Forms.RadioButton()
        Me.RadioButtonEfectivo = New System.Windows.Forms.RadioButton()
        Me.button_venta = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.numericUpDownViajan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'comboBox_origen
        '
        Me.comboBox_origen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBox_origen.FormattingEnabled = True
        Me.comboBox_origen.Location = New System.Drawing.Point(13, 49)
        Me.comboBox_origen.Name = "comboBox_origen"
        Me.comboBox_origen.Size = New System.Drawing.Size(121, 21)
        Me.comboBox_origen.TabIndex = 0
        '
        'comboBox_destino
        '
        Me.comboBox_destino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBox_destino.FormattingEnabled = True
        Me.comboBox_destino.Location = New System.Drawing.Point(140, 49)
        Me.comboBox_destino.Name = "comboBox_destino"
        Me.comboBox_destino.Size = New System.Drawing.Size(121, 21)
        Me.comboBox_destino.TabIndex = 1
        '
        'textBox_unitario
        '
        Me.textBox_unitario.Location = New System.Drawing.Point(286, 49)
        Me.textBox_unitario.Name = "textBox_unitario"
        Me.textBox_unitario.Size = New System.Drawing.Size(100, 20)
        Me.textBox_unitario.TabIndex = 2
        '
        'textBox_total
        '
        Me.textBox_total.Location = New System.Drawing.Point(286, 85)
        Me.textBox_total.Name = "textBox_total"
        Me.textBox_total.Size = New System.Drawing.Size(100, 20)
        Me.textBox_total.TabIndex = 3
        '
        'textBox_efectivo
        '
        Me.textBox_efectivo.Location = New System.Drawing.Point(286, 163)
        Me.textBox_efectivo.Name = "textBox_efectivo"
        Me.textBox_efectivo.Size = New System.Drawing.Size(100, 20)
        Me.textBox_efectivo.TabIndex = 4
        '
        'textBox_vuelto
        '
        Me.textBox_vuelto.Location = New System.Drawing.Point(286, 200)
        Me.textBox_vuelto.Name = "textBox_vuelto"
        Me.textBox_vuelto.Size = New System.Drawing.Size(100, 20)
        Me.textBox_vuelto.TabIndex = 5
        '
        'button_vuelto
        '
        Me.button_vuelto.Location = New System.Drawing.Point(205, 200)
        Me.button_vuelto.Name = "button_vuelto"
        Me.button_vuelto.Size = New System.Drawing.Size(75, 23)
        Me.button_vuelto.TabIndex = 6
        Me.button_vuelto.Text = "Vuelto"
        Me.button_vuelto.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(191, 166)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Pago en Efectivo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(227, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "TOTAL"
        '
        'numericUpDownViajan
        '
        Me.numericUpDownViajan.Location = New System.Drawing.Point(152, 91)
        Me.numericUpDownViajan.Name = "numericUpDownViajan"
        Me.numericUpDownViajan.Size = New System.Drawing.Size(56, 20)
        Me.numericUpDownViajan.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(57, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Cuantos viajan"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButtonCredito)
        Me.GroupBox1.Controls.Add(Me.RadioButtonRedCompra)
        Me.GroupBox1.Controls.Add(Me.RadioButtonEfectivo)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 163)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(126, 100)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Forma de Pago"
        '
        'RadioButtonCredito
        '
        Me.RadioButtonCredito.AutoSize = True
        Me.RadioButtonCredito.Location = New System.Drawing.Point(7, 68)
        Me.RadioButtonCredito.Name = "RadioButtonCredito"
        Me.RadioButtonCredito.Size = New System.Drawing.Size(58, 17)
        Me.RadioButtonCredito.TabIndex = 2
        Me.RadioButtonCredito.TabStop = True
        Me.RadioButtonCredito.Text = "Credito"
        Me.RadioButtonCredito.UseVisualStyleBackColor = True
        '
        'RadioButtonRedCompra
        '
        Me.RadioButtonRedCompra.AutoSize = True
        Me.RadioButtonRedCompra.Location = New System.Drawing.Point(7, 44)
        Me.RadioButtonRedCompra.Name = "RadioButtonRedCompra"
        Me.RadioButtonRedCompra.Size = New System.Drawing.Size(81, 17)
        Me.RadioButtonRedCompra.TabIndex = 1
        Me.RadioButtonRedCompra.TabStop = True
        Me.RadioButtonRedCompra.Text = "RedCompra"
        Me.RadioButtonRedCompra.UseVisualStyleBackColor = True
        '
        'RadioButtonEfectivo
        '
        Me.RadioButtonEfectivo.AutoSize = True
        Me.RadioButtonEfectivo.Location = New System.Drawing.Point(7, 20)
        Me.RadioButtonEfectivo.Name = "RadioButtonEfectivo"
        Me.RadioButtonEfectivo.Size = New System.Drawing.Size(64, 17)
        Me.RadioButtonEfectivo.TabIndex = 0
        Me.RadioButtonEfectivo.TabStop = True
        Me.RadioButtonEfectivo.Text = "Efectivo"
        Me.RadioButtonEfectivo.UseVisualStyleBackColor = True
        '
        'button_venta
        '
        Me.button_venta.Location = New System.Drawing.Point(131, 270)
        Me.button_venta.Name = "button_venta"
        Me.button_venta.Size = New System.Drawing.Size(224, 23)
        Me.button_venta.TabIndex = 12
        Me.button_venta.Text = "GENERAR VENTA"
        Me.button_venta.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 30)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Origen"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(140, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Destino"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(286, 30)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Unitario"
        '
        'VentaPasajes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(459, 322)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.button_venta)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.numericUpDownViajan)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.button_vuelto)
        Me.Controls.Add(Me.textBox_vuelto)
        Me.Controls.Add(Me.textBox_efectivo)
        Me.Controls.Add(Me.textBox_total)
        Me.Controls.Add(Me.textBox_unitario)
        Me.Controls.Add(Me.comboBox_destino)
        Me.Controls.Add(Me.comboBox_origen)
        Me.Name = "VentaPasajes"
        Me.Text = "Venta de Pasajes"
        CType(Me.numericUpDownViajan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents comboBox_origen As System.Windows.Forms.ComboBox
    Friend WithEvents comboBox_destino As System.Windows.Forms.ComboBox
    Friend WithEvents textBox_unitario As System.Windows.Forms.TextBox
    Friend WithEvents textBox_total As System.Windows.Forms.TextBox
    Friend WithEvents textBox_efectivo As System.Windows.Forms.TextBox
    Friend WithEvents textBox_vuelto As System.Windows.Forms.TextBox
    Friend WithEvents button_vuelto As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents numericUpDownViajan As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents button_venta As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents RadioButtonCredito As RadioButton
    Friend WithEvents RadioButtonRedCompra As RadioButton
    Friend WithEvents RadioButtonEfectivo As RadioButton
End Class
