﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AccesoSistema
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.textBox_cuenta = New System.Windows.Forms.TextBox()
        Me.textBox_password = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.button_access = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'textBox_cuenta
        '
        Me.textBox_cuenta.Location = New System.Drawing.Point(113, 52)
        Me.textBox_cuenta.Name = "textBox_cuenta"
        Me.textBox_cuenta.Size = New System.Drawing.Size(153, 20)
        Me.textBox_cuenta.TabIndex = 0
        '
        'textBox_password
        '
        Me.textBox_password.Location = New System.Drawing.Point(113, 96)
        Me.textBox_password.Name = "textBox_password"
        Me.textBox_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.textBox_password.Size = New System.Drawing.Size(153, 20)
        Me.textBox_password.TabIndex = 1
        Me.textBox_password.UseSystemPasswordChar = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Cuenta"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 103)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Contraseña"
        '
        'button_access
        '
        Me.button_access.Location = New System.Drawing.Point(113, 146)
        Me.button_access.Name = "button_access"
        Me.button_access.Size = New System.Drawing.Size(153, 23)
        Me.button_access.TabIndex = 4
        Me.button_access.Text = "Acceso"
        Me.button_access.UseVisualStyleBackColor = True
        '
        'AccesoSistema
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 273)
        Me.Controls.Add(Me.button_access)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.textBox_password)
        Me.Controls.Add(Me.textBox_cuenta)
        Me.Name = "AccesoSistema"
        Me.Text = "Acceso a Sistema"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents textBox_cuenta As System.Windows.Forms.TextBox
    Friend WithEvents textBox_password As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents button_access As System.Windows.Forms.Button

End Class
